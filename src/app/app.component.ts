import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import * as firebase from 'firebase';


const config = {
  apiKey: "AIzaSyALohhHfc0E33NC0MSUuHioNsmiWGEZD3E",
  authDomain: "banco-2235d.firebaseapp.com",
  databaseURL: "https://banco-2235d.firebaseio.com",
  projectId: "banco-2235d",
  storageBucket: "banco-2235d.appspot.com",
  messagingSenderId: "438192518473"
};


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    firebase.initializeApp(config);
  }
}

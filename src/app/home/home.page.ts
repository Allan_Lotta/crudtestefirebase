import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  infos = [];
  ref = firebase.database().ref('infos/');
  constructor(public router: Router/*, public loadingController: LoadingController*/) {
    this.ref.on('value', resp => {
      this.infos = [];
      this.infos = snapshotToArray(resp);
    });
  }
  addInfo() {
    this.router.navigate(['/add-info']);
  };
  edit(key) {
    this.router.navigate(['/edit/'+key]);
  };
  async delete(key) {
    firebase.database().ref('infos/'+key).remove();
  }
  
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};